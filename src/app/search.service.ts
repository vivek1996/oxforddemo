import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
@Injectable()
export class SearchService {
  url = 'https://od-api.oxforddictionaries.com:443/api/v1/search/en?q=';

  constructor(private http: HttpClient) {}

  search(value: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        app_id: 'cb35ef20',
        app_key: '603be016c344650e6d8c1da15330f7af'
      })
    };
    const _url = this.url + `${value}&prefix=false`;
    return this.http.get(_url, httpOptions);
  }
  getDefinition(word: string) {
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     Accept: 'application/json',
    //     app_id: 'cb35ef20',
    //     app_key: '603be016c344650e6d8c1da15330f7af'
    //   })
    // };
    // return this.http.get(
    //   'https://od-api.oxforddictionaries.com/api/v1/entries/en/' + word,
    //   {
    //     headers: new HttpHeaders()
    //       .set('Accept', 'application/json')
    //       .set('app_id', 'cb35ef20')
    //       .set('app_key', '603be016c344650e6d8c1da15330f7af')
    //     // ,observe: 'response'
    //   }
    // );
    // .catch((error: any) => {
    //   console.log('http error: ', error);
    //   return Observable.empty();
    // });
    // const headers = new HttpHeaders();
    // headers.append('Accept', 'application/json')
    // headers.append('app_id', 'cb35ef20')
    // headers.append('app_key', '603be016c344650e6d8c1da15330f7af');
    const myHeaders = new Headers({
      Accept: 'application/json',
      app_id: 'cb35ef20',
      app_key: '603be016c344650e6d8c1da15330f7af'
    });
    const url = `https://od-api.oxforddictionaries.com/api/v1/entries/en/${word}`;
    return this.http.get(url);
  }

  // getdata(data: string) {
  //   const headers = new HttpHeaders()
  //     .set('Accept', 'application/json')
  //     .set('app_id', 'cb35ef20')
  //     .set('app_key', '603be016c344650e6d8c1da15330f7af');
  //   const url = `https://od-api.oxforddictionaries.com/api/v1/entries/en/${data}`;
  //   return this.http.get(url, { headers });
  // }
  getdata(data: string) {
    const _headers = new HttpHeaders();
    const headers: HttpHeaders = _headers
      .append('Accept', 'application/json')
      .append('app_id', 'cb35ef20')
      .append('app_key', '603be016c344650e6d8c1da15330f7af');
    // const url = `https://od-api.oxforddictionaries.com/api/v1/entries/en/${data}`;
   // return this.http.get(url, { headers });
     return this.http.get(`/oxfordapi/search/en?q=${data}&prefix=false&limit=10`, { headers });
  }
}
