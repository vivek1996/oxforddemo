import { Component } from '@angular/core';
import { SearchService } from './search.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = '';
  data: {} = '';
  constructor(private http: HttpClient, private _http: SearchService) {}
  search(value: string) {
    this._http.getdata(value).subscribe(data => {
      console.log(data);
      this.data = data;
    });
    // this.http.get('/ace')
    //   .subscribe(response => {
    //     console.log(response);
    // })
  }
}
