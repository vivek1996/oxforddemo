import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { SearchService } from './search.service';
import { AppRoutingModule } from './router.module';

import { HomeComponent } from './home/home.component';
import { AccountComponent } from './account/account.component';
import { CallbackComponent } from './callback/callback.component';
import { AuthService } from './auth/auth.service';
@NgModule({
  imports: [BrowserModule, FormsModule, HttpClientModule, AppRoutingModule],
  declarations: [
    HelloComponent,
    AppComponent,
    HomeComponent,
    AccountComponent,
    CallbackComponent
  ],
  bootstrap: [AppComponent],
  providers: [SearchService, AuthService]
})
export class AppModule {}
